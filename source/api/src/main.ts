import { NestFactory } from '@nestjs/core';
import { getConnection } from 'typeorm';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  //create swagger
  // config swagger
  const configSwagger = new DocumentBuilder()
    .setTitle('Blog API Document')
    .setDescription('API dec')
    .setVersion('1.0.0')
    .addTag('cast')
    .build();

  const document = SwaggerModule.createDocument(app, configSwagger);
  SwaggerModule.setup('api', app, document);

  await app.listen(3000, () => {
    console.log('Server on: http://localhost:3000');
    var isStatus: boolean = getConnection().isConnected;
    if (isStatus) {
      console.log('DB connect success');
    } else {
      console.log('DB error');
    }
  });
}
bootstrap();

// list sv: netstat -ano|findstr "PID :3000"
// kill process: taskkill /pid 18264 /f
